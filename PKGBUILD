# Maintainer: slonkazoid <alifurkanyildiz@gmail.com>
# Original Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>
# Original Maintainer: T.J. Townsend <blakkheim@archlinux.org>
# Contributor: Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Thorsten Töpper <atsutane-tu@freethoughts.de>
# Contributor: Thayer Williams <thayer@archlinux.org>
# Contributor: Jeff 'codemac' Mickey <jeff@archlinux.org>

pkgname=dmenu-patched
_pkgname=dmenu
pkgver=5.2
pkgrel=1
pkgdesc='Generic menu for X'
url='https://tools.suckless.org/dmenu/'
arch=('x86_64')
license=('MIT')
depends=('sh' 'glibc' 'coreutils' 'libx11' 'libxinerama' 'libxft' 'freetype2' 'fontconfig' 'libfontconfig.so')
conflicts=(dmenu)
replaces=(dmenu)
provides=(dmenu)
source=(https://dl.suckless.org/tools/dmenu-${pkgver}.tar.gz)
source+=(https://tools.suckless.org/dmenu/patches/line-height/dmenu-lineheight-5.2.diff)
sha512sums=('7be9bb76b11225ec86a30e643e9b884f6d13af581526085212cb7401383d448a72fe4dffc3ce84ffb8319dbf36ca8384597a89070cd27d39d5618f239a2da6e3')
sha512sums+=('fd7f2cecec3205334d3de4709200ea4f4cbebea247aa6a36d99df3b9c166e848f46ef77ddfe3f6adbf49eb33937a89c1c09098c9d5b76146b18bc240da936b26')
b2sums=('f827f0d0d935f9da8103dbb73ed8b67818f1213d2f8e5a406de5e8c9496ea0479b4ccb50d3a29004abc1e4e679cac177163c02458ed144d323bb50fc120f936d')
b2sums+=('7e63d95e1d74732ec53a0f3cb9e6fb023acbdc4eba35916fd426ae245e441aa7a862f8785b240ba0cd006dcf6190676e40f35fa65931ba358c96f1ac3b2aa6de')

prepare() {
  cd ${_pkgname}-${pkgver}
  echo "CPPFLAGS+=${CPPFLAGS}" >> config.mk
  echo "CFLAGS+=${CFLAGS}" >> config.mk
  echo "LDFLAGS+=${LDFLAGS}" >> config.mk
  patch --forward --strip=1 --input="${srcdir}/dmenu-lineheight-5.2.diff"
}

build() {
  cd ${_pkgname}-${pkgver}
  make \
	  X11INC=/usr/include/X11 \
	  X11LIB=/usr/lib/X11 \
	  FREETYPEINC=/usr/include/freetype2
}

package() {
  cd ${_pkgname}-${pkgver}
  make PREFIX=/usr DESTDIR="${pkgdir}" install
  install -Dm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${_pkgname}"
}

# vim: ts=2 sw=2 et:
